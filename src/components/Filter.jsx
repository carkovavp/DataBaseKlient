import React from 'react';
import moment from 'moment';

export default class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.getHour = this.getHour.bind(this)
        this.getDay = this.getDay.bind(this)
        this.getYesterday = this.getYesterday.bind(this)
        this.getThreeDays = this.getThreeDays.bind(this)
       
    }
    
    getHour(){
        
        this.props.clickOnHour()
       
        this.props.checkSelectShop(this.props.selectStore)
              
        const x_authorization = window.location.search.split('=')[1];
        const datStart = moment().utc().add(-1,'H').format('YYYY-MM-DDTHH:mm:ss.SSS') + '+0000';
        const datEnd = moment().utc().format('YYYY-MM-DDTHH:mm:ss.SSS') + '+0000';
        let url = null;
        if(this.props.selectStore == null)
        {
            url = `https://api.evotor.ru/api/v1/inventories/stores/${this.props.storeUuid[0]}/documents?types=SELL&gtCloseDate=${datStart}&ltCloseDate=${datEnd}`
        }
        else{
        url =`https://api.evotor.ru/api/v1/inventories/stores/${this.props.selectStore}/documents?types=SELL&gtCloseDate=${datStart}&ltCloseDate=${datEnd}`
        }
        console.log(url)
        this.props.request(x_authorization, url)
    }

    getDay(){
        this.props.clickOnDay()
 
        this.props.checkSelectShop(this.props.selectStore)

        const x_authorization = window.location.search.split('=')[1];
        const datStart = moment().utc().format('YYYY-MM-DD')+'T00:00:00.000' + '+0000';
        const datEnd = moment().utc().add(1,'d').format('YYYY-MM-DD')+'T00:00:00.000' + '+0000';
        let url = null;
        if(this.props.selectStore == null){
         url =`https://api.evotor.ru/api/v1/inventories/stores/${this.props.storeUuid[0]}/documents?types=SELL&gtCloseDate=${datStart}&ltCloseDate=${datEnd}`
        }
        else{
            url =`https://api.evotor.ru/api/v1/inventories/stores/${this.props.selectStore}/documents?types=SELL&gtCloseDate=${datStart}&ltCloseDate=${datEnd}`
        }
        console.log(url)
        this.props.request(x_authorization, url)
    }

    getYesterday(){
       
        this.props.clickOnYesterday()
       
        this.props.checkSelectShop(this.props.selectStore)

        const x_authorization = window.location.search.split('=')[1];
        const datStart = moment().utc().add(-1,'d').format('YYYY-MM-DD')+'T00:00:00.000' + '+0000';
        const datEnd = moment().utc().format('YYYY-MM-DD')+'T00:00:00.000' + '+0000';
        let url = null;
        if(this.props.selectStore == null){
            url =`https://api.evotor.ru/api/v1/inventories/stores/${this.props.storeUuid[0]}/documents?types=SELL&gtCloseDate=${datStart}&ltCloseDate=${datEnd}`
            
        }
        else{
       url =`https://api.evotor.ru/api/v1/inventories/stores/${this.props.selectStore}/documents?types=SELL&gtCloseDate=${datStart}&ltCloseDate=${datEnd}`
        }
        console.log(url)
        this.props.request(x_authorization, url)
    }

    getThreeDays(){
     
        this.props.clickOn3Days()
        this.props.checkSelectShop(this.props.selectStore)

        const x_authorization = window.location.search.split('=')[1];
        const datStart = moment().utc().add(-3,'d').format('YYYY-MM-DD')+'T00:00:00.000' + '+0000';
        const datEnd = moment().utc().format('YYYY-MM-DD')+'T00:00:00.000' + '+0000';
        let url = null;
        if(this.props.selectStore == null)
        {
            url =`https://api.evotor.ru/api/v1/inventories/stores/${this.props.storeUuid[0]}/documents?types=SELL&gtCloseDate=${datStart}&ltCloseDate=${datEnd}`
            
        }
        else{
        url =`https://api.evotor.ru/api/v1/inventories/stores/${this.props.selectStore}/documents?types=SELL&gtCloseDate=${datStart}&ltCloseDate=${datEnd}`
        }
        console.log(url)
        this.props.request(x_authorization, url)
    }

    render(){
        return(
            <div className='butDays'>
            <a className='hour' onClick = {this.getHour} style={{color:this.props.colorH, fontWeight: this.props.fontWeightH }}>Час</a>
            <a onClick = {this.getDay} style={{color:this.props.colorT, fontWeight: this.props.fontWeightT }}>Сегодня</a>
            <a onClick = {this.getYesterday} style={{color:this.props.colorY, fontWeight: this.props.fontWeightY }}>Вчера</a>
            <a onClick = {this.getThreeDays} style={{color:this.props.color3, fontWeight: this.props.fontWeight3 }} >3 дня</a>
            </div>
        )
    }
}
