import React from 'react';
import superagent from 'superagent';
import moment from 'moment';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import 'react-toggle-switch/dist/css/switch.min.css' ;
import Filter from './Filter';
import Switch from 'react-toggle-switch'


export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.getResult = this.getResult.bind(this)
        this.request = this.request.bind(this)
        this.checkSelectShop = this.checkSelectShop.bind(this)
        this.clickOnToggle = this.clickOnToggle.bind(this)
        this.clickOnHour = this.clickOnHour.bind(this)
        this.clickOnDay = this.clickOnDay.bind(this)
        this.clickOnYesterday = this.clickOnYesterday.bind(this)
        this.clickOn3Days = this.clickOn3Days.bind(this)
        this.state={
            stores:[],
            extrasName: [],
            extPhone:[],
            extCard:[],
            extComment:[],
            date: [],
            number: [],
            change: [],
            summ:[],
            selectStore: null,
            timeZone: [],
            isToggleOn: true,
            storeUuid:[],
            colorH: '#06258b',
            fontWeightH:'normal',
            colorT: '#06258b' ,
            fontWeightT:'normal' ,
            colorY: '#06258b',
            fontWeightY: 'normal',
            color3: '#06258b',
            fontWeight3: 'normal',
            fontWeightLabelR: '600',
            fontWeightLabelL:'normal'
        }
    }

    componentWillMount() {
        const x_authorization = window.location.search.split('=')[1];
        const url = 'https://api.evotor.ru/api/v1/inventories/stores/search';
        superagent
            .get(url)
            .set('x-authorization', x_authorization)
            .then((response) => {
                const res1 = response.body;
                console.log(res1) 
                this.setState({
                    stores: res1,
                    storeUuid: res1.map(item=>item.uuid)
                    })
                
                });         
    }
    
    componentDidUpdate(nextProps, nextState){
        if (nextState.storeUuid != this.state.storeUuid){
            const x_authorization = window.location.search.split('=')[1]; 
            console.log(this.state.storeUuid)   
            const url2 = `https://api.evotor.ru/api/v1/inventories/stores/${this.state.storeUuid[0]}/documents?types=SELL`;
            this.request(x_authorization, url2)
        }
        console.log(nextState.storeUuid != this.state.storeUuid)
    }

    request(x_authorization, url){
        superagent
        .get(url)
        .set('x-authorization', x_authorization)
        .then((response) => {
            const res = response.body;
            console.log(res); 
            if(this.state.isToggleOn){

                function isApp(item) {
                    return item.extras['21e0e383-88fd-4176-9a09-4937594198d0'] != undefined;
                } 

                const extRes = res.filter(isApp)
                console.log(extRes.length)
                if (extRes.length == 0){
                    this.setState({
                        extrasName: ['-'],
                        extPhone:  ['-'],
                        extCard: ['-'],
                        extComment: ['-'],
                        date: ['-'],
                        number: ['-'],
                        change: ['-'],
                        summ: ['-']    
                    });
                }else{
                    this.setState({
                        extrasName: extRes.map(item => item.extras['21e0e383-88fd-4176-9a09-4937594198d0'].name),
                        extPhone: extRes.map(item => item.extras['21e0e383-88fd-4176-9a09-4937594198d0'].phone),
                        extCard: extRes.map(item => item.extras['21e0e383-88fd-4176-9a09-4937594198d0'].card),
                        extComment: extRes.map(item => item.extras['21e0e383-88fd-4176-9a09-4937594198d0'].comment),
                        date: extRes.map(item => item.closeDate),
                        number: extRes.map(item => item.number),
                        change: extRes.map(item => item.sessionNumber),
                        summ: extRes.map(item => item.closeResultSum),
                        timeZone: extRes.map(item => item.transactions[0].timezone/3600000),           
                    }); 
                }
            } else {
                
                if (res.length == 0){
                    this.setState({
                        extrasName: ['-'],
                        extPhone:  ['-'],
                        extCard: ['-'],
                        extComment: ['-'],
                        date: ['-'],
                        number: ['-'],
                        change: ['-'],
                        summ: ['-']    
                    });
                }else{
                    this.setState({
                        extrasName: res.map(item =>{ 
                                        if(item.extras['21e0e383-88fd-4176-9a09-4937594198d0'] == undefined)
                                        {
                                            return '-'
                                        }else{
                                            return item.extras['21e0e383-88fd-4176-9a09-4937594198d0'].name
                                        }
                                    }),
                        extPhone: res.map(item => {
                                        if(item.extras['21e0e383-88fd-4176-9a09-4937594198d0'] == undefined)
                                        {
                                            return '-'
                                        }else{
                                          return item.extras['21e0e383-88fd-4176-9a09-4937594198d0'].phone
                                        }
                                    }),
                        extCard: res.map(item =>{
                                        if(item.extras['21e0e383-88fd-4176-9a09-4937594198d0'] == undefined)
                                        {
                                            return '-'
                                        }else{
                                           return item.extras['21e0e383-88fd-4176-9a09-4937594198d0'].card
                                        }
                                    }),
                        extComment: res.map(item =>{
                                        if(item.extras['21e0e383-88fd-4176-9a09-4937594198d0'] == undefined)
                                        {
                                            return '-'
                                        }else{
                                           return item.extras['21e0e383-88fd-4176-9a09-4937594198d0'].card
                                        }
                                    }),
                        date: res.map(item => item.closeDate),
                        number: res.map(item => item.number),
                        change: res.map(item => item.sessionNumber),
                        summ: res.map(item => item.closeResultSum),
                        timeZone: res.map(item => item.transactions[0].timezone/3600000),           
                    }); 


                }
            }

        });
    }

    checkSelectShop(selectStores){
        if (selectStores == null){
            this.setState({
                extrasName: ['-'],
                extPhone:  ['-'],
                extCard:['-'],
                extComment:['-'],
                date: ['-'],
                number: ['-'],
                change: ['-'],
                summ: ['-']    
            })
        }
    }

    getResult(event){
        this.setState({
            colorH: '#06258b',
            fontWeightH: 'normal',
            colorT: '#06258b' ,
            fontWeightT:'normal' ,
            colorY: '#06258b',
            fontWeightY: 'normal',
            color3: '#06258b',
            fontWeight3: 'normal'
        })
        const store = event.target.value
        this.setState({
            selectStore: store
        })
                          
        const x_authorization = window.location.search.split('=')[1];    
        const url2 = `https://api.evotor.ru/api/v1/inventories/stores/${store}/documents?types=SELL`;
        this.request(x_authorization, url2)
    }

    clickOnToggle(){
        this.setState(prevState => ({
            isToggleOn: !prevState.isToggleOn
          }));
          this.setState({
            colorH: '#06258b',
            fontWeightH: 'normal',
            colorT: '#06258b' ,
            fontWeightT:'normal' ,
            colorY: '#06258b',
            fontWeightY: 'normal',
            color3: '#06258b',
            fontWeight3: 'normal'
        })

        if (this.state.isToggleOn){
            this.setState({
                fontWeightLabelR: 'normal',
                fontWeightLabelL:'600'
                
            })
        } else {
            this.setState({
                fontWeightLabelR: '600',
                fontWeightLabelL:'normal'
            })
        }

          const x_authorization = window.location.search.split('=')[1];
          let urll = 'null';
          console.log('store' +this.state.selectStore)
          if(this.state.selectStore == null)
          {
              
            urll = `https://api.evotor.ru/api/v1/inventories/stores/${this.state.storeUuid[0]}/documents?types=SELL`;
          }    
          else {
            urll = `https://api.evotor.ru/api/v1/inventories/stores/${this.state.selectStore}/documents?types=SELL`;
          }
          this.request(x_authorization, urll)
    }

    clickOnHour(){
        this.setState({
            colorH: 'black',
            fontWeightH: '500',
            colorT: '#06258b' ,
            fontWeightT:'normal' ,
            colorY: '#06258b',
            fontWeightY: 'normal',
            color3: '#06258b',
            fontWeight3: 'normal'
        })
    }

    clickOnDay(){
        this.setState({
            colorT: 'black',
            fontWeightT: '500',
            colorH: '#06258b',
            fontWeightH:'normal',
            colorY: '#06258b',
            fontWeightY: 'normal',
            color3: '#06258b',
            fontWeight3: 'normal'
        })
    }

    clickOnYesterday(){
        this.setState({
            colorY: 'black',
            fontWeightY: '500',
            colorH: '#06258b',
            fontWeightH:'normal',
            colorT: '#06258b' ,
            fontWeightT:'normal' ,
            color3: '#06258b',
            fontWeight3: 'normal'
        })
    }

    clickOn3Days(){
        this.setState({
            color3: 'black',
            fontWeight3: '500',
            colorH: '#06258b',
            fontWeightH:'normal',
            colorT: '#06258b' ,
            fontWeightT:'normal' ,
            colorY: '#06258b',
            fontWeightY: 'normal',
          
        })
    }
      

    render() {
        console.log('render2')
        const listOfShops = this.state.stores.map((item,i)=> {
            return <option className='storesName' value = {item.uuid} key={i}>{item.name}</option>
        });
        const listOfDate= this.state.date.map((item,i)=> {
            if (item == '-'){
                return item
            }
            else {return moment(item,'YYYY-MM-DD').format('DD-MM-YYYY')} 
        });
        const listOfNumber= this.state.number.map((item,i)=> {
            return item
        });
        const listOfTime= this.state.date.map((item,i)=> {
            if(item == '-'){
                return item
            } 
            else {return Number(item.substring(11,13))+ Number(this.state.timeZone[i])+ item.substring(13,16)}
        });
        const listOfChange= this.state.change.map((item,i)=> {
            return item
        });
        const  listOfSum= this.state.summ.map((item,i)=> {
            return item
        });
        const  listOfName= this.state.extrasName.map((item,i)=> {
            return item
        });
        const listOfPhone= this.state.extPhone.map((item,i)=> {
            return item
        });
        const  listOfCard= this.state.extCard.map((item,i)=> {
            return item
        });
        const listOfComment= this.state.extComment.map((item,i)=> {
            return item
        });

        const leng = listOfDate.length;
        const data = []
        
        for (var i=0; i<leng; i++){
            data.unshift({listOfDate:listOfDate[i],
                    listOfTime: listOfTime[i],
                    listOfChange: listOfChange[i],
                    listOfNumber: listOfNumber[i],
                    listOfSum: listOfSum[i],
                    listOfName: listOfName[i],
                    listOfPhone: listOfPhone[i],
                    listOfCard: listOfCard[i],
                    listOfComment: listOfComment[i]
            });        
        }
        console.log(data)

        return (
            <div>
            <div className='buttons'>
            <select className='store' onChange={this.getResult}>
                {listOfShops}
            </select>
            <label style={{fontWeight: this.state.fontWeightLabelL}}>Все чеки</label>
          
                <Switch  onClick ={this.clickOnToggle} on={this.state.isToggleOn}/>
                
            <label style = {{fontWeight: this.state.fontWeightLabelR}}>База клиентов</label>
            
            <Filter
            storeUuid = {this.state.storeUuid}
            checkSelectShop = {this.checkSelectShop} 
            selectStore = {this.state.selectStore}
            request = {this.request}
            clickOnDay = {this.clickOnDay}
            clickOnHour = {this.clickOnHour}
            clickOnYesterday = {this.clickOnYesterday}
            clickOn3Days = {this.clickOn3Days}
            colorH = {this.state.colorH}
            fontWeightH = {this.state.fontWeightH}
            colorT = {this.state.colorT}
            fontWeightT = {this.state.fontWeightT}
            colorY = {this.state.colorY}
            fontWeightY = {this.state.fontWeightY}
            color3 = {this.state.color3}
            fontWeight3 = {this.state.fontWeight3}/>
            </div>
            
            <ReactTable className='Table'
                data = {data}
                columns={[
                    {Header: 'Дата',
                     accessor:'listOfDate'},
                    {Header: 'Время',
                     accessor:'listOfTime'},
                    {Header: 'Смена',
                    accessor:'listOfChange'},
                    {Header: 'Номер чека',
                     accessor:'listOfNumber'},                  
                    {Header: 'Сумма чека',
                     accessor:'listOfSum'},
                    {Header: 'Имя клиента',
                     accessor:'listOfName'},
                    {Header: 'Номер телефона клиента',
                     accessor:'listOfPhone'},
                    {Header: 'Номер карты клиента',
                     accessor:'listOfCard'},
                    {Header: 'Комментарий',
                     accessor:'listOfComment'}
                ]}
                SubComponent={index => {
                                return(
                                    <div style={{
                                        float:'right',
                                        width: '300px',
                                        padding:'15px',
                                        fontSize:'13px'}}>
                                        {
                                            index.row.listOfName== '-'|| index.row.listOfName== ''
                                            ? 'Имя клиента: нет данных ':`Имя клиента: ${index.row.listOfName}`
                                        }<br/><br/>
                                        {
                                            index.row.listOfPhone== '-'|| index.row.listOfPhone== ''
                                            ? 'Номер телефона: нет данных ':`Номер телефона: ${index.row.listOfPhone}` 
                                        }<br/><br/>
                                        {
                                            index.row.listOfCard== '-'|| index.row.listOfCard== ''
                                            ? 'Номер карты: нет данных ':`Номер карты: ${index.row.listOfCard}`    
                                        }<br/><br/>
                                        {
                                            index.row.listOfComment== '-'|| index.row.listOfComment== ''
                                            ? 'Комментарий: нет данных ':`Комментарий: ${index.row.listOfComment}`  
                                        }

                                      
                                    </div>
                                )
                            }
                }
                defaultPageSize = {15}
                pageSizeOptions = {[5, 15, 30, 50, 100]}
                pageText = "Страницы"
                previousText= 'Предыдущая'
                nextText= 'Следующая'
                loadingText= 'Загрузка...'
                noDataText= 'Нет данных'
                ofText= 'Из'
                rowsText= 'Строк'
                
            />
            </div>
        );
    }
}