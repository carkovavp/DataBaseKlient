import React from 'react';
import { render } from 'react-dom';
import Main from './components/Main';
import './styles.css';

const reactRoot = document.getElementById('root');

render(<Main />, reactRoot);